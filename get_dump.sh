#!/bin/bash

docker exec -i db mysqldump -uroot -proot --databases wordpress --skip-comments > "dbdata/areas_dump.sql" || echo "CANNOT STORE CONTAINER DB DUMP. Database container does not running yet...";