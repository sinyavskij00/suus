#!/bin/bash

docker-compose down;
docker stop $(docker ps -aq) || echo "NO CONTAINER TO STOP, SKIPPING...";
docker rm $(docker ps -aq) || echo "NO CONTAINER TO REMOVE, SKIPPING...";
docker-compose up -d;
docker cp . wordpress:/var/www/html;